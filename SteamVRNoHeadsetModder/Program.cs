﻿using System;
using System.IO;
using Microsoft.Win32;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace SteamVRNoHeadsetModder
{
    class Program
    {
        private const string DRIVERS_DEFAULT_VRSETTINGS = "/steamapps/common/SteamVR/drivers/null/resources/settings/default.vrsettings";
        private const string DEFAULT_VRSETTINGS = "/steamapps/common/SteamVR/resources/settings/default.vrsettings";

        static void Main(string[] args)
        {
            string steamPath = (string)Registry.GetValue("HKEY_LOCAL_MACHINE\\SOFTWARE\\Wow6432Node\\Valve\\Steam", "InstallPath", "");
            if(steamPath == "")
            {
                Console.WriteLine("Steam could not be found on this machine!");
                return;
            }
            Console.WriteLine("Steam-Installation at {0}", steamPath);
            Console.WriteLine("drivers default.vrsettings exists {0}", File.Exists(steamPath + DRIVERS_DEFAULT_VRSETTINGS));
            Console.WriteLine("default.vrsettings exists {0}", File.Exists(steamPath + DEFAULT_VRSETTINGS));
            Console.WriteLine("");
            Console.WriteLine("------------------------------------------------");
            Console.WriteLine("");

            Console.WriteLine("Patch to:\n - [N]oHMD-Mode \n - [H]MD-Mode");
            string input = Console.ReadLine().ToLower();

            switch (input)
            {
                case "n":
                    PatchToNoHmd(steamPath);
                    break;
                case "h":
                    PatchToHmd(steamPath);
                    break;
                default:
                    Console.WriteLine("Whuuut?");
                    break;
            }

            Console.ReadLine();
        }

        private static void PatchToNoHmd(string steamPath)
        {
            Console.WriteLine("Patching drivers default.vrsettings...");
            using (StreamReader reader = new StreamReader(steamPath + DRIVERS_DEFAULT_VRSETTINGS))
            {
                dynamic defaultvrsettings = JValue.Parse(reader.ReadToEnd());
                reader.Close();
                defaultvrsettings.driver_null.enable = true;

                using (StreamWriter writer = new StreamWriter(steamPath + DRIVERS_DEFAULT_VRSETTINGS, false))
                {
                    writer.Write(defaultvrsettings.ToString());
                }
            }
            Console.WriteLine("Patching default.vrsettings...");
            using (StreamReader reader = new StreamReader(steamPath + DEFAULT_VRSETTINGS))
            {
                dynamic defaultvrsettings = JValue.Parse(reader.ReadToEnd());
                reader.Close();
                defaultvrsettings.steamvr.requireHmd = false;
                defaultvrsettings.steamvr.forcedDriver = "null";
                defaultvrsettings.steamvr.activateMultipleDrivers = true;

                using (StreamWriter writer = new StreamWriter(steamPath + DEFAULT_VRSETTINGS, false))
                {
                    writer.Write(defaultvrsettings.ToString());
                }
            }
            Console.WriteLine("Completed. You can now quit the application!");
        }

        private static void PatchToHmd(string steamPath)
        {
            Console.WriteLine("Patching drivers default.vrsettings...");
            using (StreamReader reader = new StreamReader(steamPath + DRIVERS_DEFAULT_VRSETTINGS))
            {
                dynamic defaultvrsettings = JValue.Parse(reader.ReadToEnd());
                reader.Close();
                defaultvrsettings.driver_null.enable = false;

                using (StreamWriter writer = new StreamWriter(steamPath + DRIVERS_DEFAULT_VRSETTINGS, false))
                {
                    writer.Write(defaultvrsettings.ToString());
                }
            }

            Console.WriteLine("Patching default.vrsettings...");
            using (StreamReader reader = new StreamReader(steamPath + DEFAULT_VRSETTINGS))
            {
                dynamic defaultvrsettings = JValue.Parse(reader.ReadToEnd());
                reader.Close();
                defaultvrsettings.steamvr.requireHmd = true;
                defaultvrsettings.steamvr.forcedDriver = "";
                defaultvrsettings.steamvr.activateMultipleDrivers = false;

                using (StreamWriter writer = new StreamWriter(steamPath + DEFAULT_VRSETTINGS, false))
                {
                    writer.Write(defaultvrsettings.ToString());
                }
            }
            Console.WriteLine("Completed. You can now quit the application!");
        }
    }
}
